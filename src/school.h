#ifndef school_H
#define school_H
#include "vector"
using namespace std;
const string positions[]{"Teacher", "Student", "Janitor", "Cleaning"};
struct subjects
{
    bool math = false;
    bool english = false;
    bool norwegian = false;
    bool naturalsciences = false;
};
class person
{
public:
    void printInformation();
    string getInfo();
    int ID;
    string name;
    string pos;
    int age;

    template <class B>
    void serialize(B &buf) const
    {
        buf << pos << name << age << ID;
    }
    template <class B>
    void parse(B &buf)
    {
        buf >> pos >> name >> age >> ID;
    }
};
class teacher : public person
{
public:
    void setSalery(int salery) { this->salery = salery; }
    void printInformation();
    string getInfo();
    subjects sub;

private:
    int salery;
};

class student : public person
{
public:
    void printInformation();
    string getInfo();
    subjects sub;
};
class janitor : public person
{
public:
    void setSalery(int salery) { this->salery = salery; }
    int getSalery() { return salery; }
    string getInfo();
    void printSalery();

private:
    int salery;
};
class cleaning : public person
{
public:
    void setSalery(int salery) { this->salery = salery; }
    int getSalery() { return salery; }
    string getInfo();
    void printSalery();

private:
    int salery;
};
#endif
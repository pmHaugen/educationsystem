#include <iostream>
#include <vector>
#include "school.h"
#include "hps/hps.h"
#include <fstream>
using namespace std;

class manager
{
public:
    void userManager(vector<person *> personArr, string path);
    void savetoFile(string path, vector<person *> personArr, bool bOverwrite);
    void changeUserInfo(string path, vector<person *> personArr);
    void clearScreen();
    void setSalery(person *personPtr);
    vector<person *> readFile(string path);
};
#include <iostream>
#include "manager.h"

void manager::userManager(vector<person *> personArr, string path)
{
    clearScreen();
    bool bManaging = true;
    while (bManaging)
    {
        cout << "Pick one of these actions: " << endl;
        cout << "(1) List all users. (2) Manage User. (3)Add User (5)Exit User Manager" << endl;

        int actionIndex;
        cin >> actionIndex;

        switch (actionIndex)
        {
        case 1:
            clearScreen();
            for (int i = 0; i < personArr.size(); i++)
            {
                cout << " | "
                     << "Nr " << i + 1 << ": " << personArr[i]->name;
            }
            cout << endl;
            break;
        case 2:
        {
            changeUserInfo(path, personArr);
            break;
        }
        case 3:
        {
            cout << "Select role: (1)Teacher, (2)Student, (3)Janitor, (4)Cleaning" << endl;
            int role = 0;
            vector<person *> tempPerson;
            cin >> role;
            if (cin.fail())
            {
                cout << "ERROR WRONG INPUT ERROR" << endl;
                bManaging = true;
                break;
            }
            switch (role)
            {
            case 1:
                tempPerson.push_back(new teacher);
                tempPerson[0]->pos = positions[0];
                break;
            case 2:
                tempPerson.push_back(new student);
                tempPerson[0]->pos = positions[1];
                break;
            case 3:
                tempPerson.push_back(new janitor);
                tempPerson[0]->pos = positions[2];
                break;
            case 4:
                tempPerson.push_back(new cleaning);
                tempPerson[0]->pos = positions[3];
                break;
            }
            cout << "Enter name: ";
            cin >> tempPerson[0]->name;
            cout << "Enter age: ";
            cin >> tempPerson[0]->age;
            tempPerson[0]->ID = personArr.size() + 1;
            savetoFile(path, tempPerson, false);
            personArr = readFile(path);
            break;
        }
        case 5:
            bManaging = false;
        }
    }
}
void manager::changeUserInfo(string path, vector<person *> personArr)
{
    cout << "Enter name to search for a user (all lowercase letters)" << endl;

    string searchName;
    cin >> searchName;
    student *tempStudent;
    teacher *tempTeacher;

    bool bFoundUser = false;
    for (int i = 0; i < personArr.size(); i++)
    {
        if (personArr[i]->name == searchName)
        {
            cout << "User information: " << endl;
            if (personArr[i]->pos == positions[0])
            {
                tempTeacher = static_cast<teacher *>(personArr[i]);
                tempTeacher->printInformation();
                cout << "Do you want to change the salery of " << personArr[i]->name << "? (1)Yes (2)No" << endl;
                int saleryInput;
                cin >> saleryInput;
                if (saleryInput == 1)
                    setSalery(personArr[i]);
            }
            else if (personArr[i]->pos == positions[1])
            {
                tempStudent = static_cast<student *>(personArr[i]);
                tempStudent->printInformation();
            }
            else
            {
                setSalery(personArr[i]);
                savetoFile(path, personArr, true);
                personArr = readFile(path);
                break;
            }
            bFoundUser = true;

            cout << "Perform action to the user information:" << endl;
            cout << "(1)Add/remove Course (2)Delete User (3)Cancel" << endl;
            int userAction;
            cin >> userAction;
            switch (userAction)
            {
            case 1:
            {
                cout << "(1)Add or (2)Remove?" << endl;
                int addOrRemoveIndex;
                cin >> addOrRemoveIndex;
                bool add = true;
                if (addOrRemoveIndex == 2)
                    add = false;
                cout << "Select course to add/remove to" << personArr[i]->name << endl;
                cout << "(1)Math. (2)English. (3)Norwegian. (4)Natural Sciences (5)Quit" << endl;
                int input;
                bool bQuit = false;
                while (!bQuit)
                {
                    cin >> input;
                    switch (input)
                    {
                    case 1:

                        if (personArr[i]->pos == positions[0])
                        {
                            tempTeacher->sub.math = add;
                        }
                        else if (personArr[i]->pos == positions[1])
                        {
                            tempStudent->sub.math = add;
                        }
                        cout << "Added Math" << endl;
                        break;
                    case 2:
                        if (personArr[i]->pos == positions[0])
                        {
                            tempTeacher->sub.english = add;
                        }
                        else if (personArr[i]->pos == positions[1])
                        {
                            tempStudent->sub.english = add;
                        }
                        cout << "Added English" << endl;
                        break;
                    case 3:
                        if (personArr[i]->pos == positions[0])
                        {
                            tempTeacher->sub.norwegian = add;
                        }
                        else if (personArr[i]->pos == positions[1])
                        {
                            tempStudent->sub.norwegian = add;
                        }
                        cout << "Added Norwegian" << endl;
                        break;
                    case 4:
                        if (personArr[i]->pos == positions[0])
                        {
                            tempTeacher->sub.naturalsciences = add;
                        }
                        else if (personArr[i]->pos == positions[1])
                        {
                            tempStudent->sub.naturalsciences = add;
                        }
                        cout << "Added Natural Sciences" << endl;
                        break;
                    case 5:
                        bQuit = true;
                        break;
                    }
                }
                savetoFile(path, personArr, true);
                personArr = readFile(path);
                i = personArr.size();
                break;
            }

            case 2:
                personArr.erase(personArr.begin() + i);
                savetoFile(path, personArr, true);
                break;
            case 3:
                break;
            }
        }
    }
    if (bFoundUser == false)
    {
        clearScreen();
        cout << "Could not find user!" << endl;
    }
}
void manager::setSalery(person *personPtr)
{
    janitor *janitorPtr;
    cleaning *cleaningPtr;
    teacher *teacherPtr;
    personPtr->printInformation();
    cout << "Enter new salery: " << endl;
    int salery;
    cin >> salery;

    if (personPtr->pos == positions[2])
    {
        janitorPtr = static_cast<janitor *>(personPtr);
        janitorPtr->setSalery(salery);
    }
    else if (personPtr->pos == positions[3])
    {
        cleaningPtr = static_cast<cleaning *>(personPtr);
        cleaningPtr->setSalery(salery);
    }
    else if (personPtr->pos == positions[0])
    {
        teacherPtr = static_cast<teacher *>(personPtr);
        teacherPtr->setSalery(salery);
    }
}
void manager::savetoFile(string path, vector<person *> personArr, bool bOverwrite)
{
    vector<string> data;
    ofstream dataFile;

    teacher *teacherPtr;
    student *studentPtr;
    janitor *janitorPtr;
    cleaning *clearningPtr;
    if (bOverwrite == true)
    {
        dataFile.open(path, ios::binary);
    }
    else if (bOverwrite == false)
    {
        dataFile.open(path, ios::binary | ios::app);
    }

    for (int i = 0; i < personArr.size(); i++)
    {
        vector<string> data;
        if (personArr[i]->pos == positions[0])
        {
            teacherPtr = static_cast<teacher *>(personArr[i]);
            data.push_back(teacherPtr->getInfo());
        }
        else if (personArr[i]->pos == positions[1])
        {
            studentPtr = static_cast<student *>(personArr[i]);
            data.push_back(studentPtr->getInfo());
        }
        else if (personArr[i]->pos == positions[2])
        {
            janitorPtr = static_cast<janitor *>(personArr[i]);
            data.push_back(janitorPtr->getInfo());
        }
        else if (personArr[i]->pos == positions[3])
        {
            clearningPtr = static_cast<cleaning *>(personArr[i]);
            data.push_back(clearningPtr->getInfo());
        }
        auto serialized = hps::to_string(data);
        dataFile << serialized << endl;
    }
    dataFile.close();
}
vector<person *> manager::readFile(string path)
{
    vector<person *> personArr;
    vector<string> data;
    string line;
    ifstream dataFile;
    student *studentPtr;
    teacher *teacherPtr;
    janitor *janitorPtr;
    cleaning *cleaningPtr;
    dataFile.open(path, ios::binary);
    if (dataFile.is_open())
    {
        std::string dataRead;
        while (getline(dataFile, line))
        {
            data = hps::from_string<vector<string>>(line);
            string newdata = data[0];
            string word;
            data.clear();
            int wordLocation = 0;
            for (int i = 0; i < newdata.length(); i++)
            {
                word += newdata.at(i);
                wordLocation++;
                if (word.at(wordLocation - 1) == ' ' || word.at(wordLocation - 1) == '.')
                {
                    word.pop_back();
                    data.push_back(word);
                    wordLocation = 0;
                    word.clear();
                }
            }
            if (data[0] == positions[0])
            {
                personArr.push_back(new teacher);
                teacherPtr = static_cast<teacher *>(personArr.back());
                teacherPtr->setSalery(stoi(data[4]));
                teacherPtr->sub.math = stoi(data[5]);
                teacherPtr->sub.english = stoi(data[6]);
                teacherPtr->sub.norwegian = stoi(data[7]);
                teacherPtr->sub.naturalsciences = stoi(data[8]);
            }
            else if (data[0] == positions[1])
            {
                personArr.push_back(new student);
                studentPtr = static_cast<student *>(personArr.back());
                studentPtr->sub.math = stoi(data[4]);
                studentPtr->sub.english = stoi(data[5]);
                studentPtr->sub.norwegian = stoi(data[6]);
                studentPtr->sub.naturalsciences = stoi(data[7]);
            }

            else if (data[0] == positions[2])
            {
                personArr.push_back(new janitor);
                janitorPtr = static_cast<janitor *>(personArr.back());
                janitorPtr->setSalery(stoi(data[4]));
            }
            else if (data[0] == positions[3])
            {
                personArr.push_back(new cleaning);
                cleaningPtr = static_cast<cleaning *>(personArr.back());
                cleaningPtr->setSalery(stoi(data[4]));
            }
            personArr.back()->pos = data[0];
            personArr.back()->name = data[1];
            personArr.back()->age = stoi(data[2]);
            personArr.back()->ID = stoi(data[3]);
        }

        dataFile.close();
    }
    else
    {
        cout << "File read error!" << endl;
    }
    return personArr;
}

void manager::clearScreen()
{
// CLEARING SCREEN
#if defined _WIN32
    system("cls");
#elif defined(__LINUX__) || defined(__gnu_linux__) || defined(__linux__)
    system("clear");
#endif
}
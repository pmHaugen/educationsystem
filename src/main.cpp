#include <iostream>
#include <fstream>
#include "school.h"
#include "manager.h"
#include <vector>

using namespace std;

int main()
{
    vector<person *> personArr;
    manager mngr;
    bool bExit = false;
    string path = "data.txt";
    personArr = mngr.readFile(path);
    cout << "Welcome to School administration" << endl;
    mngr.userManager(personArr, path);
    mngr.clearScreen();
}
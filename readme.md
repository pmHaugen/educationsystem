# N-Queen Problem solution generator
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Education System. This is a program that can load and save information about different enteties in a education system.
The program uses multiple classes and they all get stored in a vector of a parent type. The system has multiple types of courses, work positions and stores
unique information for each entity. 

## Install

![alt picture](nchange.png)

Use the command:
```
make
```
to make the run file.


## Usage

When running this program you should start off by making a bunch of users to test of the program. You can then edit each user with salary or give them courses

To run you should run the command:

```
make run
```



## Contributing



Vipps me on +47 9#######


## License


Embedded2022 @ Noroff
